const keys = [
  { dataKey: 65, textKey: 'a', sound: 'clap' },
  { dataKey: 83, textKey: 's', sound: 'hihat' },
  { dataKey: 68, textKey: 'd', sound: 'kick' },
  { dataKey: 70, textKey: 'f', sound: 'openHat' },
  { dataKey: 71, textKey: 'g', sound: 'boom' },
  { dataKey: 72, textKey: 'h', sound: 'ride' },
  { dataKey: 74, textKey: 'j', sound: 'snare' },
  { dataKey: 75, textKey: 'k', sound: 'tom' },
  { dataKey: 76, textKey: 'l', sound: 'tink' },
]

export default keys
