import React from 'react'

import './style.css'

import keys from '../../data/keys'

import KeyListItem from '../KeyListItem'

const KeyList = () => (
  <ul className="key-list">
    {keys.map(({ dataKey, textKey, sound }) => (
      <KeyListItem
        key={`${dataKey}-${textKey}`}
        dataKey={dataKey}
        textKey={textKey}
        sound={sound}
      />
    ))}
  </ul>
)

export default KeyList
