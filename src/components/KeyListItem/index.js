import React from 'react'

import './style.css'

const KeyListItem = ({ dataKey, textKey, sound }) => (
  <li className="key-list__item" data-key={dataKey}>
    <kbd className="key-list__item__text">{textKey}</kbd>
    <span className="key-list__item__sound">{sound}</span>
  </li>
)

export default KeyListItem
