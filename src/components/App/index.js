import KeyList from '../KeyList'
import './style.css'

const App = () => (
  <div class="container">
    <KeyList />
  </div>
)

export default App
